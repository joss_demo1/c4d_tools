"""
TTT v0.1
	1. Match 2 hierarchies with partial name matching.
	2. Transfer tags src -> dst.
"""
import logging
import pprint
from difflib import SequenceMatcher
from functools import partial

import c4d

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - "%(name)s" (%(filename)s:%(lineno)d), %(levelname)s\t:"%(message)s"')
LOG = logging.getLogger(__name__)


def ppd(dic, out=0):
	"""Format dict items or keys or values"""
	strs = None
	if out == 0:
		strs = [f"{k}:\t\t{dic[k]}" for k in dic.keys()]
	elif out == 1:
		strs = [str(k) for k in dic.keys()]
	elif out == 2:
		strs = [f"{dic[k]}" for k in dic.keys()]
	all_strings = "\n".join(strs)
	return all_strings


# uescape_tool.py
class Dialog(c4d.gui.GeDialog):
	Title = "TTT: Tag Transfer Tool"
	LE_MSG = 1000
	LE_IN = 1001
	LE_OUT = 1002
	B_REFRESH = 1010
	B_APPLY = 1011
	B_CANCEL = 1012
	# gui.MessageDialog('Please select an object')
	col1 = None
	obMapping = {}

	def color(self, cid):
		col = self.GetColorRGB(cid)
		return c4d.Vector(col["r"], col["g"], col["b"]) ^ c4d.Vector(1.0 / 255)

	def transfer(self):
		for src in self.objMapping.keys():
			transferTags2(src, self.objMapping[src])
		c4d.EventAdd()

	def __init__(self):
		super(Dialog, self).__init__(self)
		self.AddGadget(c4d.DIALOG_NOMENUBAR, 0)
		self.objMapping = {}
		self.col1 = c4d.Vector(0.17, 0.20, 0.23)

	def CreateLayout(self):
		fullfit = c4d.BFH_SCALEFIT | c4d.BFV_SCALEFIT
		self.SetTitle(self.Title)
		self.GroupBorderSpace(4, 4, 4, 4)
		self.GroupBegin(0, fullfit, cols=3, rows=0)
		self.AddButton(self.B_REFRESH, c4d.BFH_LEFT, name="Refresh")
		self.AddMultiLineEditText(self.LE_IN, fullfit, style=c4d.DR_MULTILINE_MONOSPACED)
		self.AddMultiLineEditText(self.LE_OUT, fullfit, style=c4d.DR_MULTILINE_MONOSPACED)
		self.GroupEnd()
		self.GroupBegin(0, c4d.BFH_SCALEFIT, rows=1)
		self.AddStaticText(0, c4d.BFH_SCALEFIT)
		self.AddButton(self.B_APPLY, 0, name="Apply")
		self.AddButton(self.B_CANCEL, 0, name="Cancel")
		self.GroupEnd()
		# self.SetDefaultColor(self.LE_OUT, c4d.COLOR_BGEDIT, self.color(c4d.COLOR_BGFOCUS))
		self.SetDefaultColor(self.LE_OUT, c4d.COLOR_BGEDIT, self.col1)
		return True

	def InitValues(self):
		self.SetString(self.LE_IN, "")
		self.SetString(self.LE_OUT, "")
		self.SetString(self.B_REFRESH, "Refresh")
		self.SetString(self.B_APPLY, "Apply")
		self.SetString(self.B_CANCEL, "Cancel")
		self.Command(self.B_REFRESH, 0)
		return True

	def Command(self, widget, msg):
		if widget == self.B_REFRESH:
			self.objMapping = getHierMapping()
			msg1 = "Select 2 different hierarchies please"
			msg2 = ""
			if self.objMapping:
				msg1 = ppd(self.objMapping, 1)
				msg2 = ppd(self.objMapping, 2)
			self.SetString(self.LE_IN, msg1)
			self.SetString(self.LE_OUT, msg2)
			return True
		elif widget == self.B_APPLY:
			self.transfer()
			self.Close()
			return True
		elif widget == self.B_CANCEL:
			self.Close()
			return True
		return False


def lsHier(inobj):
	"""Take object and try to go deeper
	Return list of objects including the starting one
	# allChildren = GetAllChildren(selection,[]) #Get all the objects
	# deleteTex = obj.KillTag(5616)
	"""

	def goDeeper(obj):
		# recursive func
		res = []
		while obj:
			res.append(obj)
			res += goDeeper(obj.GetDown())
			obj = obj.GetNext()
		return res

	result = []
	result += goDeeper(inobj)
	return result


def ls(objs=None, hier=False):
	"""Return currently active objects + hierarchy directly below
	If objs given - returns them and hierarchy below
	"""
	doc = c4d.documents.GetActiveDocument()
	if objs is None:
		objs = doc.GetActiveObjects(c4d.GETACTIVEOBJECTFLAGS_SELECTIONORDER)
	if hier is False:
		return objs
	sel = []
	for obj in objs:
		sel.append(obj)
		sel += lsHier(obj.GetDown())
	return sel


def getObjMats(obj):
	"""Return list of materials: [mats]"""
	oname = obj.GetName()
	tags = obj.GetTags()
	mats = []
	flag = False
	# texTags = [mats.append(tag.GetMaterial().GetName()) for tag in tags if tag.CheckType(c4d.Ttexture)]
	for tag in tags:
		if not tag.CheckType(c4d.Ttexture):
			continue
		mats.append(tag.GetMaterial())
		flag = True
	# here mats have list of all object materials
	if flag:
		return mats
	return []


def getMatMapping(objs):
	"""Take OBJECTS
	Return Dict of {objName: [mats]} pairs
	"""
	mapping = {}
	for obj in objs:
		name = obj.GetName()
		mats = getObjMats(obj)
		mapping[name] = mats
	return mapping


def applyMatMapping(mapping):
	"""Take matMapping dict: {objName: [mats]} and reapply it"""
	doc = c4d.documents.GetActiveDocument()
	for oname in mapping.keys():
		obj = doc.SearchObject(oname)
		mats = mapping[oname]
		if not mats or obj is None:
			continue
		mat = mats[0]
		tag = c4d.BaseTag(c4d.Ttexture)
		tag[c4d.TEXTURETAG_MATERIAL] = mat
		obj.InsertTag(tag)
		print(mat, tag, obj)
	c4d.EventAdd()


def getCommonString(A, B):
	"""Return largest common string"""
	match = SequenceMatcher(None, A, B).find_longest_match(0, len(A), 0, len(B))
	return A[match.a : (match.a + match.size)]


def matchHierarchyNames(From, To):
	"""From, To: lists of _OBJECTS_
	mapping = {objName: [mats]}
	"""
	fromNames = [x.GetName() for x in From]
	toNames = [x.GetName() for x in To]
	mapping = {}
	for To in toNames:
		lowerTo = To.lower()
		for f, From in enumerate(fromNames):
			if (lowerTo not in From.lower()) and (From.lower() not in lowerTo):
				continue
			mapping[fromNames[f]] = To
	return mapping


def getHierMapping():
	"""Produce hierarchy mapping dict for 2 selected objects:
	{srcName: dstName}
	"""
	sel = ls(hier=False)
	if len(sel) != 2:
		return None
	From = ls([sel[0]], hier=True)
	To = ls([sel[1]], hier=True)
	objMapping = matchHierarchyNames(From, To)
	return objMapping


def transferTags(src, dst):
	"""Transfer all tags src -> dst using NAMES"""
	doc = c4d.documents.GetActiveDocument()
	srcObj = doc.SearchObject(src)
	dstObj = doc.SearchObject(dst)
	srcTags = srcObj.GetTags()
	# texTags = [mats.append(tag.GetMaterial().GetName()) for tag in tags if tag.CheckType(c4d.Ttexture)]
	doc.StartUndo()
	for tag in srcTags:
		tag2 = tag.GetClone(0)
		doc.AddUndo(c4d.UNDO_NEW, tag2)
		print("{} -> {}".format(dstObj, tag2))
		dstObj.InsertTag(tag2)
	doc.EndUndo()


def transferTags2(src, dst):
	"""Transfer all tags src -> dst using NAMES, v2"""
	doc = c4d.documents.GetActiveDocument()
	srcObj = doc.SearchObject(src)
	dstObj = doc.SearchObject(dst)
	r = srcObj.CopyTagsTo(dstObj, visible=c4d.NOTOK, variable=c4d.NOTOK, hierarchical=c4d.NOTOK)
	return r


def main():
	dlg = Dialog()
	# dlg.Open(c4d.DLG_TYPE_MODAL, defaultw = 800, defaulth = 600)
	dlg.Open(c4d.DLG_OK, defaultw=800, defaulth=600)


if __name__ == "__main__":
	main()
